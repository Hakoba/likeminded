import Vue from "vue";
import Vuex from "vuex";

import request from "./modules/request";
import auth from "./modules/auth";
import agencies from "./modules/agencies";
import events from "./modules/events";
import users from "./modules/users";
import stages from "@/store/modules/stages";

// import someEntity from "./modules/someEntity";
Vue.use(Vuex);

export default new Vuex.Store({
  namespaced: true,
  modules: {
    Auth: auth,
    Request: request,
    Agencies: agencies,
    Events: events,
    Users: users,
    Stages: stages
  },
  state: {
   
  },
  mutations: {
  
  },
  getters: {},
  actions: {
   
  }
});
