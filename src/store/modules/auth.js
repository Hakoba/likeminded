// auth and selfuser / user settings

import Vue from "vue";
import router from "../../router/";
import {
  AUTH_ABOUT_ME,
  AUTH_LOGIN,
  AUTH_REGISTRATION,
  AUTH_LOGOUT,
  AUTH_PASSWORD_CHANGE,
  AUTH_PASSWORD_RESET,
  AUTH_PASSWORD_SET
} from "../const/auth";
import { HTTP_POST, HTTP_GET } from "../const/request";

export default {
  namespaced: true,
  state: {
    userToken: localStorage.getItem("userToken") || "",
    user: {
      email: "Loading....",
      id: 10000000000000,
      name: "Loading...",
      roles: []
    },
    drawerMiniVariant: true
  },
  getters: {
    getAccessToken: state => state.userToken,
    getUser: state => {
      const { email, id, name } = state.user;
      return {
        email,
        id,
        name
      };
    },
    getUserId: state => state.user?.id,
    myRole: state => state.user?.role?.name
  },
  mutations: {
    toggleMiniDrawer: (state, type) => {
      if (type == "full") {
        state.drawerMiniVariant = false;
        localStorage.setItem("drawerStyle", type);
      } else {
        state.drawerMiniVariant = true;
        localStorage.setItem("drawerStyle", type);
      }
    },
    [AUTH_LOGIN]: (state, { data }) => {
      Vue.set(state, "userToken", data.access_token);
      localStorage.setItem("userToken", data.access_token);
    },
    [AUTH_REGISTRATION]: (state, { data }) => {
      Vue.set(state, "userToken", data.access_token);
      localStorage.setItem("userToken", data.access_token);
    },
    [AUTH_LOGOUT]: state => {
      localStorage.removeItem("userToken");
      Vue.set(state, "userToken", "");
      router.push({ name: "login", params: { sessionError: true } });
    },
    [AUTH_ABOUT_ME]: (state, { data }) => {
      state.user = data[0];
    }
  },
  actions: {
    [AUTH_REGISTRATION]: async ({ dispatch }, body) => {
      return await dispatch(
        HTTP_POST,
        {
          method: AUTH_REGISTRATION,
          body,
          with_token: false,
          namespace: "Auth"
        },
        { root: true }
      );
    },
    [AUTH_ABOUT_ME]: async ({ dispatch }) => {
      return await dispatch(
        HTTP_GET,
        {
          method: AUTH_ABOUT_ME,
          namespace: "Auth"
        },
        { root: true }
      );
    },
    [AUTH_PASSWORD_SET]: async ({ dispatch }, body) => {
      return await dispatch(
        HTTP_POST,
        {
          method: AUTH_PASSWORD_SET,
          body,
          with_token: false,
          namespace: "Auth"
        },
        { root: true }
      );
    },
    [AUTH_PASSWORD_CHANGE]: async ({ dispatch }, body) => {
      return await dispatch(
        HTTP_POST,
        {
          method: AUTH_PASSWORD_CHANGE,
          body,
          with_token: true,
          namespace: "Auth"
        },
        { root: true }
      );
    },
    [AUTH_PASSWORD_RESET]: async ({ dispatch }, body) => {
      return await dispatch(
        HTTP_POST,
        {
          method: AUTH_PASSWORD_RESET,
          body,
          with_token: false,
          namespace: "Auth"
        },
        { root: true }
      );
    },
    [AUTH_LOGIN]: async ({ dispatch }, body) => {
      return await dispatch(
        HTTP_POST,
        {
          method: AUTH_LOGIN,
          body,
          with_token: false,
          namespace: "Auth"
        },
        { root: true }
      );
    },
    [AUTH_LOGOUT]: async ({ dispatch }) => {
      return await dispatch(
        HTTP_POST,
        {
          method: AUTH_LOGOUT,
          namespace: "Auth"
        },
        { root: true }
      );
    }
  }
};
