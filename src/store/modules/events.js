//events store namespace

/**
 * replaceWords
 * @param EVENT
 * @param Event
 * @param EVENTS
 * @param events
 * @param Events

 */

import {
  CREATE_EVENT,
  DELETE_EVENT,
  GET_EVENTS,
  GET_EVENT,
  UPDATE_EVENT,
  GET_EVENT_TYPES,
  GET_STAGES_BY_EVENT,
  TIE_USER,
  GET_USERS_BY_EVENT,
} from "../const/events";
import { HTTP_POST, HTTP_GET, HTTP_DELETE, HTTP_PATCH } from "../const/request";

const events = {
  state: () => ({
    events: [],
    currentEvent: {},
    types: [],
    eventStages: [],
    tiedUsers: [],

  }),
  actions: {
   
    async [GET_USERS_BY_EVENT]({ dispatch }, id) {
      return await dispatch(
        HTTP_GET,
        {
          method: GET_USERS_BY_EVENT,
          replace: { id },
          namespace: "Events"
        },
        { root: true }
      );
    },
    async [GET_EVENT_TYPES]({ dispatch }) {
      return await dispatch(
        HTTP_GET,

        { method: GET_EVENT_TYPES, withTrashed: true, namespace: "Events" },
        { root: true }
      );
    },
    async [GET_EVENTS]({ dispatch }) {
      return await dispatch(
        HTTP_GET,

        { method: GET_EVENTS, withTrashed: true, namespace: "Events" },
        { root: true }
      );
    },
    async [CREATE_EVENT]({ dispatch }, data) {
      return await dispatch(
        HTTP_POST,
        {
          method: CREATE_EVENT,
          body: data,

          no_commit: true,
          namespace: "Events"
        },
        { root: true }
      );
    },
    async [TIE_USER]({ dispatch }, data) {
      return await dispatch(
        HTTP_POST,
        {
          method: TIE_USER,
          body: data,

          no_commit: true,
          namespace: "Events"
        },
        { root: true }
      );
    },
    async [GET_EVENT]({ dispatch }, id) {
      return await dispatch(
        HTTP_GET,
        {
          method: GET_EVENT,
          replace: { id },
          namespace: "Events"
        },
        { root: true }
      );
    },
    
    async [GET_STAGES_BY_EVENT]({ dispatch }, id) {
      return await dispatch(
        HTTP_GET,
        {
          method: GET_STAGES_BY_EVENT,
          replace: { id },
          namespace: "Events"
        },
        { root: true }
      );
    },
    async [DELETE_EVENT]({ dispatch }, { id }) {
      return await dispatch(
        HTTP_DELETE,
        {
          method: DELETE_EVENT,
          params: { id },
          no_commit: true,
          namespace: "Events"
        },
        { root: true }
      );
    },
    async [UPDATE_EVENT]({ dispatch }, { id, data }) {
      return await dispatch(
        HTTP_PATCH,
        {
          method: UPDATE_EVENT,
          params: { id },
          body: data,
          no_commit: true,
          namespace: "Events"
        },
        { root: true }
      );
    }
  },
  mutations: {
    [GET_EVENT_TYPES](state, { data }) {
      state.types = data;
    },
    [GET_EVENTS](state, { data }) {
      state.events = data;
    },
    [GET_EVENT](state, { data }) {
      state.currentEvent = data[0];
    },
    [GET_STAGES_BY_EVENT](state,{data}){
      state.eventStages = data
    },
    [GET_USERS_BY_EVENT](state,{data}){
      state.tiedUsers = [...new Set(data.map(({ user_id: id})=> id))]
    },
  
    
  },
  getters: {
    getOnlyEvents: state =>
      state.events.map(({ id, name }) => {
        return { id, name };
      }),
    getEventsName: state => state.events.map(({ name }) => name)
    // getSomeModuleValue: state => state.someModuleValue
  },
  namespaced: true
};
export default events;
