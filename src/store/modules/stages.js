//stages store namespace

/**
 * replaceWords
 * @param STAGE
 * @param Stage
 * @param STAGES
 * @param stages
 * @param Stages

 */

import {
  CREATE_STAGE,
  DELETE_STAGE,
  GET_STAGES,
  TIE_STAGE,
  UPDATE_STAGE
} from "../const/stages";
import {
  HTTP_POST,
  HTTP_GET,
  HTTP_DELETE,
  HTTP_PATCH,
} from "../const/request";

const stages = {
  state: () => ({
    stages: [],
    currentStage: {},
  }),
  actions: {
    async [GET_STAGES]({ dispatch }) {
      return await dispatch(
        HTTP_GET,

        { method: GET_STAGES, withTrashed: true, namespace: "Stages" },
        { root: true }
      );
    },
    async [TIE_STAGE]({ dispatch }, data) {
      return await dispatch(
        HTTP_POST,
        {
          method: TIE_STAGE,
          body: data,
          no_commit: true,
          namespace: "Stages"
        },
        { root: true }
      );
    },
    async [CREATE_STAGE]({ dispatch }, data) {
      return await dispatch(
        HTTP_POST,
        {
          method: CREATE_STAGE,
          body: data,

          no_commit: true,
          namespace: "Stages"
        },
        { root: true }
      );
    },
   
    async [DELETE_STAGE]({ dispatch }, { id }) {
      return await dispatch(
        HTTP_DELETE,
        {
          method: DELETE_STAGE,
          params: { id },
          no_commit: true,
          namespace: "Stages"
        },
        { root: true }
      );
    },
    async [UPDATE_STAGE]({ dispatch }, { id, data }) {
      return await dispatch(
        HTTP_PATCH,
        {
          method: UPDATE_STAGE,
          params: { id },
          body: data,
          no_commit: true,
          namespace: "Stages"
        },
        { root: true }
      );
    }
  },
  mutations: {
    [GET_STAGES](state, { data }) {
      state.stages = data;
    },
   
   
  },
  getters: {
    getOnlyStages: state =>
      state.stages.map(({ id, name }) => {
        return { id, name };
      }),
    getStagesName: state => state.stages.map(({ name }) => name)
    // getSomeModuleValue: state => state.someModuleValue
  },
  namespaced: true
};
export default stages;
