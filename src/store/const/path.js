// import { someModule } from "module";

import {
  AUTH_ABOUT_ME,
  AUTH_LOGIN,
  AUTH_LOGOUT,
  AUTH_PASSWORD_RESET,
  AUTH_PASSWORD_CHANGE,
  AUTH_PASSWORD_SET,
  AUTH_REGISTRATION
} from "./auth";

import {
  CREATE_EVENT,
  GET_EVENT_TYPES,
  DELETE_EVENT,
  GET_EVENTS,
  GET_EVENT,
  UPDATE_EVENT,
  TIE_USER,
  GET_USERS_BY_EVENT,  
} from "./events";

import {
  GET_STAGES,
  CREATE_STAGE,
  GET_STAGES_BY_EVENT,
  DELETE_STAGE,
  ADD_STAGE,
  UNBLOCK_STAGE,
  UPDATE_STAGE,
  TIE_STAGE
} from "./stages";

import {
  CREATE_USER,
  GET_USERS,
  GET_USER,
  UPDATE_USER,
  DELETE_USER,
  REPAIR_USER,
  GET_FILTERED_USERS,
  GET_EVENT_STAGES_MAP,
  MANAGE_THIS

} from "./users";

const stages = {
  [GET_STAGES]: "stages",
  [CREATE_STAGE]: "stages",
  [TIE_STAGE]: "event/add_stage",
  [DELETE_STAGE]: "",
  [ADD_STAGE]: "event/add_stage",
  [UNBLOCK_STAGE]: "",
  [UPDATE_STAGE]: ""
};
const auth = {
  [AUTH_LOGIN]: "login",
  [AUTH_LOGOUT]: "logout",
  [AUTH_ABOUT_ME]: "users/own",
  [AUTH_REGISTRATION]: "reg/",
  [AUTH_PASSWORD_RESET]: "password-reset",
  [AUTH_PASSWORD_CHANGE]: "password-change",
  [AUTH_PASSWORD_SET]: "password-set"
};
const users = {
  [GET_USERS]: "users",
  [GET_FILTERED_USERS]: "users?:query:",
  [CREATE_USER]: "users",
  [GET_USER]: "users/:id:",
  [UPDATE_USER]: "users",
  [DELETE_USER]: "users/:id:/to-trash",
  [REPAIR_USER]: "users/:id:/from-trash",
  [GET_EVENT_STAGES_MAP]:"manage/:id:",
  [MANAGE_THIS]: 'manage'

};
const events = {
  [GET_EVENTS]: "event/own",
  [GET_EVENT_TYPES]: "event/types/",
  [GET_STAGES_BY_EVENT]: "stages/event/:id:",
  [CREATE_EVENT]: "event",
  [GET_EVENT]: "event/:id:",
  [UPDATE_EVENT]: "event/:id:",
  [DELETE_EVENT]: "event/:id:",
  [TIE_USER]: "event/add_user_by_admin",
  [GET_USERS_BY_EVENT]: "event/users/:id:" ,
  
};

export default {
  ...auth,
  ...users,
  ...events,
  ...stages
};
