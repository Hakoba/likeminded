export const GET_STAGES = "GET_STAGES";
export const CREATE_STAGE = "CREATE_STAGE";
export const DELETE_STAGE = "DELETE_STAGE";
export const TIE_STAGE = "TIE_STAGE";
export const ADD_STAGE = "ADD_STAGE";
export const UNBLOCK_STAGE = "UNBLOCK_STAGE";
export const UPDATE_STAGE = "UPDATE_STAGE";
