import Vue from "vue";
import VueRouter from "vue-router";
import errorLayout from "@/components/layouts/ErrorLayout.vue";
import authLayout from "@/components/layouts/AuthLayout.vue";
import loginPage from "@/components/views/Auth/Login.vue";
Vue.use(VueRouter);
const prefix = "LikeMinded: ";
const routes = [
  {
    path: "/error",
    component: errorLayout,
    children: [
      {
        path: "404",
        name: "404",
        meta: {
          title: "404: Не найдено"
        },
        component: () => import("@/components/views/Errors/404.vue")
      }
    ]
  },
  {
    path: "/",
    redirect: { name: "events" }
  },
  {
    path: "/auth/forgot-password",
    redirect: "/auth/password-set"
  },
  {
    path: "*",
    redirect: "/error/404"
  },

  {
    path: "/auth",
    component: authLayout,
    children: [
      {
        path: "login",
        name: "login",
        meta: { title: prefix + "Страница входа" },
        component: loginPage
      },
      {
        path: "registration",
        name: "registration",
        meta: { title: prefix + "Регистрация" },
        component: () => import("@/components/views/Auth/Register.vue")
      },
     
    ]
  },
  ///////////////////////////////////////////////////////
  //////////////////////////////////////////////////////
  /////////////////////MAIN////////////////////////////
  ////////////////////////////////////////////////////
  ///////////////////////////////////////////////////
  {
    path: "",
    meta: {
      // TODO: Сделать true
      loginGuard: false
    },
    redirect: "/main",
    component: () => import("@/components/layouts/MainLayout.vue"),
    children: [
      {
        path: "cabinet",
        name: "cabinet",
        meta: {
          title: prefix + "Кабинет"
        },
        component: () => import("@/components/views/Cabinet/index.vue")
      },
      // {
      //   path: "control",
      //   name: "control",
      //   meta: {
      //     title: prefix + "Управление пользователями"
      //   },
      //   component: () => import("@/components/views/Control/index.vue")
      //   // children: controlComponents,
      // },
      // { path: "control/*", redirect: { name: "control" } },
      // {
      //   path: "campaigns",
      //   name: "campaigns",
      //   meta: {
      //     title: prefix + "Кампании"
      //   },
      //   component: () => import("@/components/views/AdCampaigns/index.vue")
      // },
      {
        path: "/events",
        name: "events",
        meta: {
          title: prefix + "события"
        },
        component: () => import("@/components/views/Events/index.vue")
      },
      {
        path: "/events/:id",
        name: "currentEvent",
        meta: {
          title: prefix + "Соревнование"
        },
        component: () => import("@/components/views/Events/current.vue")
      },
      {
        path: "/events/:id/new",
        name: "createEvent",
        meta: {
          title: prefix + "Новое соревнование"
        },
        component: () => import("@/components/views/Events/new.vue")
      },
      {
        path: "/events/:id/edit",
        name: "editEvent",
        meta: {
          title: prefix + "Редактирование соревнование"
        },
        component: () => import("@/components/views/Events/edit.vue")
      }
    ]
  }
];

const router = new VueRouter({
  routes,
  mode: "hash" //history || hash
});
router.beforeEach(async (to, from, next) => {
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.title);

  const nearestWithMetaGuarded = to.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.loginGuard);

  if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

  if (nearestWithMetaGuarded) {
    // router guard
    if (localStorage.getItem("userToken")) {

      next();
    } else {
      setTimeout(() => {
        Vue.prototype.$notify({
          type: "warn",
          title: "Ошибка",
          text: "Необходимо войти в систему"
        });
      }, 300);
      next("/auth/login?sessionError=true");
    }
  } else {
    next();
  }
});
router.onError(error => {
  if (/loading chunk \d* failed./i.test(error.message)) {
    window.location.reload();
  }
});
export default router;
