import Vue from "vue";
import Vuetify from "vuetify/lib";
import ru from "vuetify/es5/locale/ru";
import VuetifyConfirm from "vuetify-confirm";

Vue.use(Vuetify);

const vuetify = new Vuetify({
  lang: {
    locales: { ru },
    current: "ru"
  },
  theme: {
    options: {
      customProperties: true
    },
    dark: false,
    themes: {
      light: {
        primary: "#64FFDA",
        secondary: "#808AFF", // blue // #6200EE
        info: "#2F2D2D", // dark
        accent: "#711C96", //#01A299 // #C2185B
        error: "#EB2873",
        warning: "#E2C321",
        success: "#43DE79",
        agency: "#DA3131",
        client: "#3193DA",
        brand: "#31DABB"
      },
      dark: {
        primary: "#64FFDA",
        secondary: "#808AFF", // blue // #6200EE
        info: "#2F2D2D", // dark
        accent: "#711C96", //#01A299 // #C2185B
        error: "#EB2873",
        warning: "#E2C321",
        success: "#43DE79",
        agency: "#DA3131",
        client: "#3193DA",
        brand: "#31DABB"
      }
    }
  }
});
Vue.use(VuetifyConfirm, {
  vuetify,
  buttonTrueText: "Ок",
  buttonFalseText: "Отмена",
  color: "warning",
  icon: "mdi-alert",
  title: "Подтвердите действие",
  width: 350,
  property: "$confirm",
  persistent: true
});
export default vuetify;
