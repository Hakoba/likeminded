import Vue from "vue";
Vue.prototype.$baseURL = `${process.env.VUE_APP_API_URL}${process.env.VUE_APP_API_VERSION}/`;
