import { mapGetters } from "vuex";

export default {
  computed: {
    ...mapGetters(["loading", "error", "cancel"])
  },
  methods: {
    isError(name) {
      return this.loading(name) === "error";
    },
    cancelFunc(name) {
      return this.cancel(name);
    },
    errorMessage(name) {
      return this.error(name).error || this.error(name);
    },
    /**
     * Запрос вызывает снекбар с текстом ошибки либо возвращает инфу что все хорошо
     * @param requestName - константа из vuex
     * @returns {Boolean}
     */

    checkRequestOnError(requestName) {
      if (this.isError(requestName)) {
        this.$notify({
          type: "err",
          title: "Ошибка",
          // duration: 1999999,
          text: this.errorMessage(requestName)
        });
        return false;
      }
      return true;
    }
  }
};
